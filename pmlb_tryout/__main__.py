from pandas import DataFrame, read_pickle
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn import linear_model
from sklearn.metrics import accuracy_score
from time import time
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
from pmlb import fetch_data, classification_dataset_names


class AbzuQLattice():
    def __init__(self, ):
        models = None

    def fit(self, X, y):
        df = DataFrame(X)
        df["class"] = y
        df.columns = df.columns.astype(str)
        import feyn
        ql = feyn.QLattice()
        self.model = ql.auto_run(data=df, output_name="class", kind="classification")[0]

    def predict(self, X):
        X = DataFrame(X)
        X.columns = X.columns.astype(str)
        return self.model.predict(X)

    def score(self, X, y_true):
        X = DataFrame(X)
        X.columns = X.columns.astype(str)
        y_pred = [int(round(i, 0)) for i in self.predict(X)]
        return accuracy_score(y_true, y_pred)


def get_algorithms():
    return {
        "Logistic Regression \n(with standard scaler)": make_pipeline(
            StandardScaler(), LogisticRegression(max_iter=100)
        ),
        "Random Forest\n(200 trees)": RandomForestClassifier(n_estimators=200),
        "Ridge Classifier": linear_model.RidgeClassifier(),
        "Abzu QLattice\n(with autorun)": AbzuQLattice(),
    }


def benchmark_algorithms(benchmark_path="benchmark.pkl"):
    start = time()
    print("Benchmarking algorithms")
    scores = []
    for dataset in tqdm(classification_dataset_names[:60], unit="dataset"):
        X, y = fetch_data(dataset, return_X_y=True)
        if len(set(y)) != 2:
            continue
        train_X, test_X, train_y, test_y = train_test_split(X, y)
        for algo_name, algo in get_algorithms().items():
            fit_start = time()
            algo.fit(train_X, train_y)
            fit_time = round(time()-fit_start, 2)
            scores.append(
                {
                    "Algorithm": algo_name,
                    "Score": algo.score(test_X, test_y),
                    "Dataset": dataset,
                    "Samples": X.shape[0],
                    "Features": X.shape[1],
                    "Fit Time": fit_time,
                }
            )
    scores = DataFrame(scores)
    scores.to_pickle(benchmark_path)
    print("Execution time was", round(time()-start, 2))


def plot_benchmark(benchmark_path, plot_path, x="Score", y="Algorithm"):
    print(f"Plotting benchmark {x}/{y}")
    scores = read_pickle(benchmark_path)
    sns.set_theme(style="ticks")
    f, ax = plt.subplots(figsize=(7, 6))
    sns.boxplot(
        scores,
        x=x,
        y=y,
        hue=y,
        whis=[0, 100],
        width=0.6,
        palette="vlag",
    )
    sns.stripplot(scores, x=x, y=y, size=4, color=".3")
    ax.xaxis.grid(True)
    ax.set(ylabel="")
    ax.figure.tight_layout()
    sns.despine(trim=True, left=True)
    plt.savefig(plot_path)


if __name__ == "__main__":
    benchmark_path = "benchmark.pkl"
    benchmark_algorithms(benchmark_path)
    plot_benchmark(benchmark_path, "benchmark_score.svg")
    plot_benchmark(benchmark_path, "benchmark_fittime.svg", x="Fit Time")
